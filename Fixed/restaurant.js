function formAuto(){
	
    var contactName = document.forms["contactUs"]["name"].value;
    var contactEmail = document.forms["contactUs"]["email"].value;
    var contactPhone = document.forms["contactUs"]["phone"].value;
    var reasonOther = document.forms["contactUs"]["dropDown"].value;
    var additionalInfo = document.forms["contactUs"]["info"].value;
    var M = document.forms["contactUs"]["Mon"].checked;
    var T = document.forms["contactUs"]["Tues"].checked;
    var W = document.forms["contactUs"]["Wed"].checked;
    var Th = document.forms["contactUs"]["Thurs"].checked;
    var F = document.forms["contactUs"]["Fri"].checked;



        if ( contactName == "" || contactName == null) {
            alert("Please enter your name!");
            return false;}

		if ((contactEmail == "") && (contactPhone == "")) {
		alert("Please enter either an email or a phone number!");
         return false;}

		if  ((reasonOther == "Other") && (additionalInfo == "") || ( additionalInfo == null)){
		alert("Please enter your reason in the additional information box.");
     	return false;}


     	if (M == false && T == false && W == false && Th == false && F == false){
     		alert("Please check a day(s)!");
     		return false;
     	}

}